package com.paytest.paytest.service;

import com.paytest.paytest.common.BusinessException;
import com.paytest.paytest.entity.PayOrder;
import com.baomidou.mybatisplus.extension.service.IService;
import com.paytest.paytest.entity.TotalMsg;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lijunzhan
 * @since 2021-08-21
 */
public interface IPayOrderService extends IService<PayOrder> {

    TotalMsg queryTodayTotal(Long merchantId)throws BusinessException;

    TotalMsg queryYesterdayTotal(Long merchantId)throws BusinessException;



}
