package com.paytest.paytest.service.impl;

import com.paytest.paytest.common.BusinessException;
import com.paytest.paytest.common.CommonErrorCode;
import com.paytest.paytest.entity.PayOrder;
import com.paytest.paytest.entity.TotalMsg;
import com.paytest.paytest.mapper.PayOrderMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.paytest.paytest.service.IPayOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lijunzhan
 * @since 2021-08-21
 */
@Service
public class PayOrderServiceImpl extends ServiceImpl<PayOrderMapper, PayOrder> implements IPayOrderService {

    @Autowired
    PayOrderMapper payOrderMapper;
    @Override
    public TotalMsg queryTodayTotal(Long merchantId) throws BusinessException {
        //非空校验
        if (merchantId == null){
            throw new BusinessException(CommonErrorCode.E_200202);
        }
        TotalMsg totalMsg = payOrderMapper.queryTodayTotal(merchantId);

        return totalMsg;
    }

    @Override
    public TotalMsg queryYesterdayTotal(Long merchantId) throws BusinessException {
        //非空校验
        if (merchantId == null){
            throw new BusinessException(CommonErrorCode.E_200202);
        }
        TotalMsg totalMsg = payOrderMapper.queryYesterdayTotal(merchantId);

        return totalMsg;
    }
}
