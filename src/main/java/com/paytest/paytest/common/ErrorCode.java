package com.paytest.paytest.common;

public interface ErrorCode {

    int getCode();

    String getDesc();

}
