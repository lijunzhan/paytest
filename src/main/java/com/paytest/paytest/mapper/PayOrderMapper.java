package com.paytest.paytest.mapper;

import com.paytest.paytest.entity.PayOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.paytest.paytest.entity.TotalMsg;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lijunzhan
 * @since 2021-08-21
 */
@Mapper
public interface PayOrderMapper extends BaseMapper<PayOrder> {

    /**
     * 查询今天的订单信息
     * @param merchantId
     * @return
     */

    @Select("select sum(TOTAL_AMOUNT) TotalMoney,count(ID) TotalCount,sum(TOTAL_AMOUNT)/count(ID) PersonAvgMoney " +
            "from pay_order " +
            "where TO_DAYS(PAY_SUCCESS_TIME) = TO_DAYS(NOW()) and MERCHANT_ID = #{merchantId}")
    TotalMsg queryTodayTotal(@Param("merchantId") Long merchantId);


    /**
     * 查询昨天的订单信息
     * @param merchantId
     * @return
     */

    @Select("select sum(TOTAL_AMOUNT) TotalMoney,count(ID) TotalCount,sum(TOTAL_AMOUNT)/count(ID) PersonAvgMoney " +
            "from pay_order " +
            "where TO_DAYS(PAY_SUCCESS_TIME) = TO_DAYS(NOW()) - 1 and MERCHANT_ID = #{merchantId}")
    TotalMsg queryYesterdayTotal(@Param("merchantId") Long merchantId);
}
