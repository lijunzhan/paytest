package com.paytest.paytest.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TotalMsg implements Serializable {

    //订单总金额
    Float TotalMoney;

    //订单总数
    Integer TotalCount;

    //人均销售额
    Float PersonAvgMoney;

}
