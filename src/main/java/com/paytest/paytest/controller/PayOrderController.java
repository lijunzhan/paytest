package com.paytest.paytest.controller;


import com.paytest.paytest.common.BusinessException;
import com.paytest.paytest.entity.TotalMsg;
import io.swagger.annotations.ApiImplicitParam;
import org.springframework.stereotype.Controller;
import com.paytest.paytest.service.IPayOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lijunzhan
 * @since 2021-08-21
 */
@Slf4j
@RestController
@Api(value = "商户", tags = "", description="")
public class PayOrderController {

    @Autowired
    private IPayOrderService payOrderService;

    @PostMapping("pay/select")
    @ApiOperation(value = "查询某商户今日和昨日的（营业额、订单总数、人均销售额）")
    @ApiImplicitParam(name = "merchantId",value = "商户ID")
    public Map<String, TotalMsg> queryTotal(Long merchantId) throws BusinessException{

        //根据商户id
        Map<String, TotalMsg> map = new HashMap<>();
        TotalMsg totalMsg = payOrderService.queryTodayTotal(merchantId);
        map.put("today",totalMsg);
        TotalMsg totalMsg1 = payOrderService.queryYesterdayTotal(merchantId);
        map.put("yesterday",totalMsg1);
        return map;
    }


}
